<?php

function hubbed_register_meta_boxes() {
    add_meta_box( 'hubbed-box-id','Hubbed Consignment', 'hubbed_my_display_callback', 'shop_order',"side", "high" );
}
add_action( 'add_meta_boxes', 'hubbed_register_meta_boxes' );

function hubbed_my_display_callback($post)
{
	
	$order_id = $post->ID;
	$hubbed_consignment_no = "";
	$hubbed_enable_address = get_post_meta($order_id,'hubbed_enable_address',true);
	$hubbed_consignment_no = get_post_meta( $order_id, 'hubbed_consignment_no', true);
	if ($hubbed_enable_address == 1) {
		?>
		<p class="hubbed-tracking-error error" style="color: #a00;"></p>
    <p class="plzwait-track"></p>
		<?php wp_nonce_field( 'hubbed_order_tacking_nonce', 'hubbed_order_tacking_nonce' ); ?>
		<input type="hidden" name="hubbed_enable_order_id" id="hubbed_enable_order_id" value="<?php echo $order_id; ?>">
		<label><b>Tracking No.</b></label>
		<input type="text" name="hubbed_enable_order_tacking" id="hubbed_enable_order_tacking" value="<?php echo $hubbed_consignment_no;?>" <?php if (!empty($hubbed_consignment_no)) { echo "readonly";}?> >
		<?php if (empty($hubbed_consignment_no)) { ?>
		<input type="button" class="button button-primary hubbed_order_fullfill_button" id="hubbed_order_fullfill_button" value="Fulfill Items" style="margin-top: 20px;text-align: center;padding: 2px 15px;" >

		<?php
	}
	}
}


// Generate tracking no.
add_action('wp_ajax_nopriv_hubbed_generate_consignment', 'hubbed_generate_consignment_function');
add_action('wp_ajax_hubbed_generate_consignment', 'hubbed_generate_consignment_function');

function hubbed_generate_consignment_function()
{
$response = array();

if ( ! isset( $_POST['hubbed_order_tacking_nonce'] ) || ! wp_verify_nonce( $_POST['hubbed_order_tacking_nonce'], 'hubbed_order_tacking_nonce' )) 
{
   
   $response['responseMessage'] = 'Sorry, your nonce did not verify.';
   exit;

} else {
   
   $hubbed_enable_order_id = sanitize_text_field($_POST['hubbed_enable_order_id']);
   $hubbed_enable_order_tacking = sanitize_text_field($_POST['hubbed_enable_order_tacking']);     
   $order = wc_get_order( $hubbed_enable_order_id );  
   $hubbed_consignment_no = get_post_meta( $hubbed_enable_order_id, 'hubbed_consignment_no', true);

   if (empty($hubbed_enable_order_tacking)) {
     $hubbed_enable_order_tacking = get_post_meta( $hubbed_enable_order_id, 'hubbed_client_id', true );
   }

  if($order != null && $order->get_status() == 'processing' ) 
    {
      
      $articles = array();
              $count = 0;
              
              foreach ( $order->get_items() as $item ) 
              {
                $product_id = absint($item['product_id']);
                $product = wc_get_product( $product_id );
                $productweight = $product->get_weight();
                $weight = 1;
                if ( empty($productweight) or $productweight < 1)
                 {
                  $weight = 1;
                  }else{ 
                    $weight = $product->get_weight(); 
                  }

                $articles[$count]["barcode"] = ($product->get_id().'-'.$hubbed_enable_order_id.'-'.$item ->get_quantity().'-'.$hubbed_enable_order_tacking);
                $articles[$count]["volume"]= (($item ->get_quantity() != null) ? $item ->get_quantity() : '');
                $articles[$count]["weight"]= $weight;
                $count++;
              }
                    
              
             $hubbed_address_status = get_post_meta( $hubbed_enable_order_id, 'hubbed_enable_address', true );
             $hubbed_storeDlb = get_post_meta( $hubbed_enable_order_id, 'hubbed_checkout_hubbedlb', true ); 
              
              if($hubbed_address_status == 1)
              {
                $hubbed_address_status;
                $passdata = array();
                  $passdata['store_id'] = (get_option('hubbed_store_id'));
                  $passdata['store_key'] = (get_option('hubbed_api_key'));
                  $passdata['order_id'] = absint($hubbed_enable_order_id);
                  $passdata['consignmentnumber'] = $hubbed_enable_order_tacking;
                  $passdata['storedlb'] = ($hubbed_storeDlb);
                  $passdata['articles'] = ((isset($articles) && $articles != null) ? $articles : []);
                  $passdata['contactName'] = ($order->get_billing_first_name().' '.$order->get_billing_last_name());
                  $passdata['emailaddress'] = ($order->get_billing_email());
                  $passdata['mobilenumber'] = ($order->get_billing_phone());
                  $passdata['consignmenttype'] = '1';
                  $passdata['hubbedid'] = get_post_meta( $hubbed_enable_order_id, 'hubbed_client_id', true );
                
                
               $body = hubbed_api_call('/inbound/consignment', $passdata);
                if (isset($body['result'][0]) && $body['result'][0]['responseCode'] == 200 ) {
                 update_post_meta( $hubbed_enable_order_id, 'hubbed_consignment_no',$hubbed_enable_order_tacking);

                 update_post_meta( $hubbed_enable_order_id, 'hubbed_consignment_status','success');
          
              $response['responseMessage'] = $body['result'][0]['responseMessage'];
              $response['responseCode'] = 200;
                }else{
                  update_post_meta( $hubbed_enable_order_id, 'hubbed_consignment_status','error');
                    if ($response['responseCode'] == 400) {
                      $response['responseMessage'] = $body['response'];
                    }else{
                    $response['responseMessage'] = $body['result'][0]['responseMessage'];
                      }
              }

        } else{$response['responseMessage'] = "The Click & Collect order was not sent to HUBBED successfully.";}
          
       

    } else{$response['responseMessage'] = "Order is not in processing.";}


echo json_encode($response);


}
die();
}