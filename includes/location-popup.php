<div class="hubbed-modal">
   <div class="hubbed-modal-overlay hubbed-toggle"></div>
   <div id="hubbedModelLoaderBackgroud" class="hubbed-loader-backgroud" style="display:none;">
      <div id="hubbedModalLoader" class="hubbedModalLoader"></div>
   </div>
   <div class="hubbed-center">
      <div class="hubbed-modal-wrapper">
         <div class="hubbed-close hubbed-toggle" id="hubbed-span-clear-search"><span class="dashicons dashicons-no-alt"></span>
         </div>
         <div class="hubbed-first-phase" style="padding: 65px 45px;">
            <div id="hubbed-model-one" class="hubbed-model-one">
               <div class="hubbed-logo" id="hubbed-logo">
                  <img src="https://app.hubbed.com.au/storage/wp/assets/images/logo.png">
               </div>
               <h2 class="hubbed-big-heading" id="hubbed-big-heading">SELECT A COLLECTION POINT</h2>

               <div class="hubbed-search-location">
                  <div class="hubbed-search-relative">
                     <a href="#" class="hubbed-pin-location">
                     </a>
                     <input autocomplete="off" type="text" name="postcode_search" id="hubbed-search-field-one" placeholder="Enter your postcode or suburb" class="hubbed-search-field hubbed-search-field-one hubbed-form-field-text">
                     <?php wp_nonce_field( 'search_nonce_postcode', 'search_nonce_postcode' ); ?>
                     <a href="#" class="hubbed-input-clear" id="hubbed-input-clear">
                     <img src="https://app.hubbed.com.au/storage/wp/assets/images/field-clear-icon.svg">
                     </a>                                    
                     <button class="hubbed-search-button" id="hubbed-search-button-first"></button>

                  </div>
                  <p id="hubbedErrMsg"></p>
               </div>
               <div class="hubbed-steper" id="hubbed-steper">
                                                     
                  <div class="hubbed-steper-1 hubbed-steper-part">
                                                            
                     <div class="hubbed-steper-icon hubbed-steper-icon-location"></div>
                     <h4>SEARCH</h4>
                     <p>Select a convenient pick up location</p>
                  </div>
                  <div class="hubbed-steper-2 hubbed-steper-part">
                     <div class="hubbed-steper-icon hubbed-steper-icon-send"></div>
                     <h4>CHECK OUT</h4>
                     <p>Complete your order</p>
                  </div>
                  <div class="hubbed-steper-3 hubbed-steper-part">
                     <div class="hubbed-steper-icon hubbed-steper-icon-collect"></div>
                     <h4>Collect</h4>
                     <p>Receive a collection notification and pick up your parcel</p>
                  </div>
               </div>
               <!-- / hubbed-model-one -->                     
            </div>
         </div>



         <div class="hubbed-second-phase" style="padding: 65px 45px;">
         	               <!--- map div -->
               <div id="hubbed-map"></div>
               <!--- map div -->
            <div class="hubbed-search-location">
               <div class="hubbed-search-relative"> 
                  <span class="hubbed-pin-location"></span> 
                  <input autocomplete="off" type="text"  id="hubbed-search-field-two" name="Search" placeholder="Enter your postcode or suburb" class="hubbed-search-field hubbed-search-field-two hubbed-form-field-text"> 
                  <a href="javascript:void(0)" class="hubbed-input-clear" id="hubbed-input-clear"> </a> 
                  <button class="hubbed-search-button" id="hubbed-search-button-second"></button>
               </div>
               <div class="explore-toggles">
                  <button type="button" class="search-button-toggle active" id="hubbed-explore-toggle-map" onclick="showhidemap(1)">Map</button>
                  <button type="button" class="search-button-toggle" id="hubbed-explore-toggle-list" onclick="showhidemap(0)">List</button>
               </div>
               <p id="hubbedErrMsg"></p>
            </div>
            <div class="hubbed-search-result" style="display: block;">
               <div class="hubbed-search-address-part">
                  <div id="hubbed_display_filters" class="hubbed-filter-part">
                  	 <?php
            $radius_data = array(
          'store_id'=> (get_option('hubbed_store_id')),
        
          );
      $response_radius = hubbed_api_call('/storeFilter', $radius_data);
      //print_r($response_radius);
      $radiuses = $response_radius['filtes']['Search_Radius'];
      //print_r($radiuses);
            ?>

            <div class="hubbed-radius-km"> 
              <label class="hubbed-label hubbed-h4-heading">Search Radius</label>
              <?php
              foreach ($radiuses as $radius) 
              {
                $checked="";
                if (Hubbed_DEFAULT_RADIUS == $radius) {
                  $checked = "checked";
                }

                echo '<label class="hubbed_label_value hubbed-checkbox">'.$radius.'KM<input type="radio" name="hubbed_radius" class="hubbed_radius hubbed-radio-field" value="'.$radius.'"'.$checked.' > <span class="hubbed-checkmark"></span> </label> ';
              }
              ?>
          </div>

         
                              <?php
              $services = $response_radius['filtes']['Services'];
              $channels = $response_radius['filtes']['Channels'];
               if (!empty($services) && !empty($channels)) 
               {
               	   echo '<div class="hubbed-filter-dropdown">';
               


              if (!empty($services)) 
              {
              echo '<select class="hubbed_filter_servises" name="hubbed_filter_service">';
              echo '<option value=""> Select Services </option>';
              foreach ($services as $service) 
              {
              echo '<option value="'.$service['id'].'">'.$service['name'].'</option>';  
              }
              echo '</select>';
              }


              if (!empty($channels)) 
              {
              echo '<select class="hubbed_filter_channel" name="hubbed_filter_channel">';
              echo '<option value=""> Select Channels </option>';
              foreach ($channels as $channel) 
              {
              echo '<option value="'.$channel['channel_id'].'">'.$channel['channel_name'].'</option>';  
              }
              echo '</select>';
              }
                ?>
                <div class="hubbed-filter-buton hubbed-same-width-dropdown">
                    <button class="hubbed-applay-filter hubbed-button-same" id="hubbed-filter-btn" >Filter</button>
                    <button class="hubbed-clear-filter hubbed-link-button" id="hubbed-filter-clear">Clear Filter</button>
                </div>
             </div>
         <?php }?>
           </div>


                  <div id="hubbed_display_response">
                     <div id="hubbed_display_records">

                     </div>

                     <div id="hubbed_display_showmorebtn">

                     </div>

                  </div>

               </div>
            </div>
         </div>
         <!-- / hubbed-modal-wrapper-->                              
      </div>
      <!-- / hubbed center-->                      
   </div>
</div>