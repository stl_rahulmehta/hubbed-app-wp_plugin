<?php
/*
Plugin Name: HUBBED
Plugin URI: https://hubbed.com.au
Description: HUBBED has developed a number of technology solutions that allow carriers and merchants to seamlessly integrate into the HUBBED collection point networks, and deliver various services to the delight of their customers.
Version: 2
Author: Hubbed
Author URI: http://hubbed.com.au/
*/
if ( ! defined( 'ABSPATH' ) ) exit;
define('HUBBED_APP_URL', 'https://apps.hubbed.com.au');
//define('HUBBED_APP_URL', 'https://hubbed.cartcoders.com');
define('HUBBED_WEB_URL', 'https://clickandcollect.hubbed.com/wp-json/hubbedzohoapi/v1/');
define('Hubbed_API_URL', HUBBED_APP_URL.'/api');
define('HUBBED_CLIENT_API_URL', 'https://production-api.hubbed.com.au/v1/');
define('Hubbed_DIR', plugin_dir_path(__FILE__));
define('Hubbed_URL', plugin_dir_url(__FILE__));
define('Hubbed_DEFAULT_RADIUS', 3);


//Activation Of Hubbed Plugin
 if ( ! function_exists( 'hubbed_install' ) ) {
function hubbed_install() {
  update_option('hubbed_success_install',0);
  $random_string = md5(microtime());    
  $store_id = ($random_string . substr(strftime("%Y", time()),2)); 
  /*
  if(get_option('hubbed_store_id') != null)
  {
    $store_id = get_option('hubbed_store_id');
  } 
  */
  $data = array(
                'store_id' => $store_id,
                'store_key' => (get_option('hubbed_api_key')),
                'store_url'=> (get_site_url()),
                'commerce_type' => 0,
                'store_name'=> (get_bloginfo( 'name' ))
              );
  $body = hubbed_api_call('/storeInstall', $data);
  if ($body['responseCode'] == 200 && count($body['response']) > 0) 
  {
    $response = $body['response'];
    update_option('hubbed_store_id',sanitize_text_field($response['store_id']));
    update_option('hubbed_success_install',1);
    update_option( 'hubbed_map_key', 'AIzaSyAaCq0KCZ2j9PUX4NVQrNobf7bIqgj8Mec');
    update_option( 'hubbed_key_location', 'first' );
    update_option( 'hubbed_setting_enable', '1');
  }

}
}
register_activation_hook( __FILE__, 'hubbed_install' );





// Api CALL Function
 if ( ! function_exists( 'hubbed_api_call' ) ) {
function hubbed_api_call($path, $fields)
{
  $url = Hubbed_API_URL . $path;
  $args = array(
    'body' => $fields,
    'timeout' => '30',
    'redirection' => '5',
    'httpversion' => '1.0',
    'blocking' => true,
    'headers' => array(),
    'cookies' => array()
  );

  $response = wp_remote_post($url, $args);
  $body = wp_remote_retrieve_body($response);
  return json_decode($body, true);
}
}


//hubbed Client Api Call
 if ( ! function_exists( 'hubbed_client_api_call' ) ) {
function hubbed_client_api_call($path,$fields,$type,$header)
{
          $url = esc_url_raw($path);

          
          $apidata = array(
            'body' => $fields,
            'timeout' => '45',
            'redirection' => '5',
            'httpversion' => '1.0',
            'blocking' => true,
            'sslverify'   => false,
            'headers' => $header,
            'cookies' => array(),
          );
        if($type == 'post')
        {
          $response = wp_remote_post($url, $apidata);
        }else
        {
            $response = wp_remote_get($url, $apidata);
        }
          
          $body = wp_remote_retrieve_body($response);
         
        //print_r($body); 
         $data = json_decode($body, true);
        return $data;
}
}


// Check store ID And Installation or not

if ( !empty((get_option('hubbed_store_id'))) && (get_option('hubbed_success_install')) == 1 ) 
{

  if ( !empty(get_option('hubbed_api_key'))) {

  add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true');
    add_filter( 'default_checkout_shipping_country', 'hubbed_default_checkout_shipping_country' );
    
    function hubbed_default_checkout_shipping_country() {
    return 'AU'; 
      }
    }


  if(is_admin())
  {
      require_once 'includes/hubbed_admin.php';
  }
 require_once 'includes/search_list.php';
 require_once 'includes/order-post.php';




// load admin css and script
  add_action( 'admin_enqueue_scripts', 'hubbed_load_admin_styles' );
  if ( ! function_exists( 'hubbed_load_admin_styles' ) ) {
  function hubbed_load_admin_styles() 
  {
  wp_enqueue_style( 'admin_HUBBED_css', Hubbed_URL. 'assets/admin/style.css', false, '1.0.0' );
  wp_enqueue_script( 'admin_HUBBED_js', Hubbed_URL. 'assets/admin/script.js', array('jquery'),false, '1.0.0' );
  wp_enqueue_script( 'admin_HUBBED_table_js', Hubbed_URL. 'assets/datatables.min.js', array('jquery'),false, '' );
  wp_enqueue_style( 'admin_HUBBED_table_css', Hubbed_URL. 'assets/datatables.min.css', false, '' );
  wp_enqueue_script( 'admin_HUBBED_bootstrap_js', Hubbed_URL. 'assets/bootstrap/js/bootstrap.min.js', array('jquery'),false, '' );
  wp_enqueue_style( 'admin_HUBBED_bootstrap_css', Hubbed_URL. 'assets/bootstrap/css/bootstrap.min.css',false, '' );
  
  wp_localize_script( 'admin_HUBBED_js', 'hubbed_admin_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
  }
  }
 
  // Check the mapping of API_Key, Map_key And Store id  hubbed_map_key
$storesetupdata =array(
                'store_key'=>(get_option('hubbed_api_key')),
                'store_id'=>(get_option('hubbed_store_id')),
                'store_url'=>(get_site_url()),

                );
$checkstore = hubbed_api_call('/storeSetup', $storesetupdata);

if ($checkstore['result'] == true) 
{
  
// enable or disable this features
  if ((get_option( 'hubbed_setting_enable') == 1) and !empty(get_option('hubbed_api_key'))) 
  {

// load front css and script
    add_action( 'wp_enqueue_scripts', 'hubbed_load_front_styles' );
  if ( ! function_exists( 'hubbed_load_front_styles' ) ) {
    function hubbed_load_front_styles() 
    { 
      

         $mapjs = 'https://maps.googleapis.com/maps/api/js?key='.(get_option('hubbed_map_key')); 
        wp_enqueue_style( 'front_HUBBED_css', Hubbed_URL. 'assets/front/style.css', false, '1.0.0' );
        wp_enqueue_script( 'front_HUBBED_js', Hubbed_URL. 'assets/front/script.js', array('jquery'),false, '1.0.0' );
        wp_enqueue_script( 'front_HUBBED_map_js', $mapjs,false, '1.0.0' );
        wp_localize_script( 'front_HUBBED_js', 'hubbed_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_enqueue_style( 'dashicons' );

       
    }
  }



// Display the Click & collect Button
 add_action('woocommerce_after_cart_totals', 'hubbed_delivery_option_function');
  if ( ! function_exists( 'hubbed_delivery_option_function' ) )
  {
  function hubbed_delivery_option_function()
  {
   
    echo sprintf( '<div class="wc-proceed-to-checkout input-group">
        <div class="input-group-btn">
        <button id="hubbed-btn-click-collect" class="hubbed-popup-button hubbed-toggle" type="button">%s</button>
        </div>
    </div>',esc_attr('Click & Collect'));
    // Add the hubbed popup
    
    require_once 'includes/location-popup.php';
  } 
}
} // enable / disable

} // check store id
} 



// show the hubbed address at checkout page after the billing information.
add_action('woocommerce_after_checkout_billing_form','hubbed_sethubbedaddress_checkout');
if ( ! function_exists( 'hubbed_sethubbedaddress_checkout' ) )
  {
function hubbed_sethubbedaddress_checkout()
{
session_start();

if(isset($_SESSION['hubbed_selectaddress']))
{
echo '<div class="hubbed_checkout_address">'; 
//echo '<div class="cross_hubbed_checkout_address" id="cross_hubbed_checkout_address"><span class="dashicons dashicons-no-alt"></span></div>';
echo '<p> CLICK & COLLECT Address</p>';

echo '<textarea readonly name="hubbed_checkout_address_feild">'.$_SESSION["hubbed_selectaddress"].'</textarea>';
echo'<input type="hidden" name="hubbed_checkout_storeDlb" id="ship_to_hubbed_storeDlb" value="'.$_SESSION['hubbed_hubbedlb'].'">';
echo '</div>';
}
}}



// Save the address and data during the order with hubbed
add_action('woocommerce_checkout_update_order_meta','hubbed_orderhubbed_address',20,1);
if ( ! function_exists( 'hubbed_orderhubbed_address' ) )
  {

function hubbed_orderhubbed_address($order_id)
{
  global $wpdb;
   session_start();
    $user_id = get_current_user_id();
    if(isset($_POST['hubbed_checkout_address_feild']) && !empty($_POST['hubbed_checkout_address_feild']))
  {
    //$hubbed_address = $_SESSION['hubbed_selectaddress'];
  
  
  // call Client hubbed Api for hubbed id
  $api_key = get_option('hubbed_api_key');
 $path1 = HUBBED_CLIENT_API_URL.'auth/token/';
  $apidata0 = array(
            'body' => array('hubbed_error'=>1), // hubbed_error is passed for slove the content lenth. it is dummy data.
            'timeout' => '45',
            'redirection' => '5',
            'httpversion' => '1.0',
            'blocking' => true,
            'sslverify'   => false,
            'headers' => array(
                      'APIKEY' => $api_key,
                      'Content-Type' => 'application/json',
                          ),
            'cookies' => array(),
          );

          $response = wp_remote_post($path1, $apidata0);
         // print_r($response);
          $body = wp_remote_retrieve_body($response);
          $gettoken = json_decode($body, true);
         $hubbedtoken = $gettoken['responseMessage'];
         if (empty($hubbedtoken)) {
           $response = wp_remote_post($path1, $apidata0);
          $body = wp_remote_retrieve_body($response);
          $gettoken = json_decode($body, true);
         $hubbedtoken = $gettoken['responseMessage'];
         }
    
  $path2 = HUBBED_CLIENT_API_URL.'ecommercecustomer/gethubbedid/';
  $apidata1 = array(
            'body' => array('hubbed_error'=>1), // hubbed_error is passed for slove the content lenth. it is dummy data.
            'timeout' => '45',
            'redirection' => '5',
            'httpversion' => '1.0',
            'blocking' => true,
            'sslverify'   => false,
            'headers' => array(
                      'Authorization' => 'Bearer '.$hubbedtoken,
                      'Content-Type' => 'application/json;charset=UTF-8',
                      
                          ),
            'cookies' => array(),
          );

          $response = wp_remote_get($path2, $apidata1);
         // print_r($response);
          $body = wp_remote_retrieve_body($response);
          $gethubbedid = json_decode($body, true);
          $hubbed_id = $gethubbedid['result']['success']['hubbed_id'];

          if (empty($hubbed_id)) {
          $response = wp_remote_get($path2, $apidata1);
          $body = wp_remote_retrieve_body($response);
          $gethubbedid = json_decode($body, true);
          $hubbed_id = $gethubbedid['result']['success']['hubbed_id'];  
          }


$hubbed_key_location = get_option('hubbed_key_location');


    $order = wc_get_order( $order_id );
    

    $firstname = $order->get_shipping_first_name();
    $lastname = $order->get_shipping_last_name();
    $email = $order->get_billing_email();
    if ( empty($firstname) || empty($lastname)) 
      {

      $firstname = $order->get_billing_first_name();
      $lastname = $order->get_billing_last_name();

      }

  if ($hubbed_key_location == 'first') {  
    $firstname = $firstname.'['.$hubbed_id.']';
    $ship_firstname  = get_post_meta($order_id,'_shipping_first_name',true);
    if(!empty($ship_firstname))
    {
      $ship_firstname = $ship_firstname.'['.$hubbed_id.']';
      update_post_meta($order_id,'_shipping_first_name',$ship_firstname); 
    }
  }

if ($hubbed_key_location == 'last') {  
  $lastname = $lastname.'['.$hubbed_id.']';
    $ship_lastname  = get_post_meta($order_id,'_shipping_last_name',true);
    if(!empty($ship_lastname))
    {
      $ship_lastname = $ship_lastname.'['.$hubbed_id.']';
      update_post_meta($order_id,'_shipping_last_name',$ship_lastname); 
    }
  }
if ($hubbed_key_location == 'address') { 
  $hubbed_street = $_SESSION['hubbed_address'].'['.$hubbed_id.']';

    $ship_address1  = get_post_meta($order_id,'_shipping_address_1',true);
    if(!empty($ship_address1))
    {
      $ship_address1 = $ship_address1.'['.$hubbed_id.']';
      update_post_meta($order_id,'_shipping_address_1',$ship_address1); 
    }

}else{ $hubbed_street = $_SESSION['hubbed_address']; }

if ($hubbed_key_location == 'city') { 
  $hubbed_city = $_SESSION['hubbed_city'].'['.$hubbed_id.']';
    $ship_city  = get_post_meta($order_id,'_shipping_city',true);
    if(!empty($ship_city))
    {
      $ship_city = $ship_city.'['.$hubbed_id.']';
      update_post_meta($order_id,'_shipping_city',$ship_city); 
    }

}else{ $hubbed_city = $_SESSION['hubbed_city']; }

 $hubbed_address = $firstname.' '.$lastname.'|'.$_SESSION['hubbed_company'].'|'.$hubbed_street.'|'.$hubbed_city.' , '.$_SESSION['hubbed_state'].'|'.$_SESSION['hubbed_country'].' , '. $_SESSION['hubbed_zip'];
  


  //$hubbed_address = $_SESSION['hubbed_selectadminaddress'];
  update_post_meta($order_id,'hubbed_checkout_address',$hubbed_address);
  update_post_meta($order_id,'hubbed_enable_address',1);
  update_post_meta($order_id,'hubbed_client_id', $hubbed_id);
 

  
// Call the Api during order
  if (empty($hubbed_id)) {
    $hubbed_id = "empty";
  }

      $firstname1 = $order->get_shipping_first_name();
    $lastname1 = $order->get_shipping_last_name();
    if ( empty($firstname1) || empty($lastname1)) 
      {

      $firstname1 = $order->get_billing_first_name();
      $lastname1 = $order->get_billing_last_name();

      }

  $hubbed_hubbedlb = $_POST['hubbed_checkout_storeDlb'];
    $orderdata = array();
    $passdata['store_id'] = (get_option('hubbed_store_id'));
    $passdata['store_key'] = (get_option('hubbed_api_key'));
    $passdata['order_id'] = $order_id;
    $passdata['contactName'] = $firstname1.' '.$lastname1;
    $passdata['emailaddress'] = $email;
    $passdata['hubbedid'] = $hubbed_id;
    $passdata['storedlb'] = $hubbed_hubbedlb;
    $body = hubbed_api_call('/order/create', $passdata);
update_post_meta($order_id ,'error',$body);
  }

  if(isset($_SESSION['hubbed_hubbedlb']) && isset($_POST['hubbed_checkout_storeDlb']) && !empty($_POST['hubbed_checkout_storeDlb'])){
    //$hubbed_hubbedlb = $_SESSION["hubbed_hubbedlb"];
  $hubbed_hubbedlb = $_POST['hubbed_checkout_storeDlb'];
  update_post_meta($order_id,'hubbed_checkout_hubbedlb',$hubbed_hubbedlb);
}
 
 
}}



// Show the hubbed address and data at order page or post.
  add_action( 'woocommerce_admin_order_data_after_shipping_address', 'hubbed_consignment_response' );
  
  function hubbed_consignment_response( $order )
  {  

    if(get_post_meta($order->id,'hubbed_enable_address',true)==1)
    {

?>
  
      <br class="clear" />
      <p style="margin-bottom: 0px;"><b>Hubbed Details</b></p>
        <?php 
        //$hubbed_consignment_response = json_decode(get_post_meta( $order->id, 'hubbed_consignment_response', true ));
        $hubbed_address = (get_post_meta( $order->id, 'hubbed_checkout_address', true ));
        $hubbed_address_array = explode('|',$hubbed_address);
        echo '<div class="orderhubbedaddress">';
        foreach ($hubbed_address_array as $hubbed_addres) {
          
          echo $hubbed_addres.'<br/>';
        }
        $hubbed_storeDlb = (get_post_meta( $order->id, 'hubbed_checkout_hubbedlb', true ));
        ?>
        <br/>
          <p><b>Hubbed store Dlb - </b><?php echo $hubbed_storeDlb; ?></p>  
           <p style="margin-bottom:0px;"><b>Consignment No. - </b><?php echo (get_post_meta( $order->id, 'hubbed_consignment_no', true )); ?></p> 
        
        </div>
        <?php
  }
}



  add_action('woocommerce_thankyou', 'hubbed_thankyou_page_address', 10, 1);
  function hubbed_thankyou_page_address( $order_id ) {

      session_start();
    if(isset($_SESSION["hubbed_selectaddress"])){
        unset($_SESSION["hubbed_selectaddress"]);
          }
  if(isset($_SESSION['hubbed_address'])){ unset($_SESSION['hubbed_address']);}
  if(isset($_SESSION['hubbed_city'])){ unset($_SESSION['hubbed_city']);}
  if(isset($_SESSION['hubbed_state'])){ unset($_SESSION['hubbed_state']);}
   
   unset($_SESSION['hubbed_country']);
   unset($_SESSION['hubbed_zip']);
    if(isset($_SESSION['hubbed_hubbedlb'])){
        unset($_SESSION['hubbed_hubbedlb']);
    }

  global $woocommerce;
        $woocommerce->customer->set_shipping_first_name('');
        $woocommerce->customer->set_shipping_last_name('');
        $woocommerce->customer->set_shipping_company('');
        $woocommerce->customer->set_shipping_address_1('');
        $woocommerce->customer->set_shipping_address_2('');
        $woocommerce->customer->set_shipping_country('');
        $woocommerce->customer->set_shipping_state('');
        $woocommerce->customer->set_shipping_city('');
        $woocommerce->customer->set_shipping_postcode('') ;

 /*echo (get_option('hubbed_store_id'));
 print_r(get_post_meta($order_id ,'error',true));
  */
  }



// Event on Processed to checkut page
  add_action('woocommerce_proceed_to_checkout', 'woocommerce_proceed_to_checkout_function');
  function woocommerce_proceed_to_checkout_function() {
      session_start();
    if(isset($_SESSION["hubbed_selectaddress"])){
        unset($_SESSION["hubbed_selectaddress"]);
          }
  if(isset($_SESSION['hubbed_address'])){ unset($_SESSION['hubbed_address']);}
  if(isset($_SESSION['hubbed_city'])){ unset($_SESSION['hubbed_city']);}
  if(isset($_SESSION['hubbed_state'])){ unset($_SESSION['hubbed_state']);}
   
   unset($_SESSION['hubbed_country']);
   unset($_SESSION['hubbed_zip']);
    if(isset($_SESSION['hubbed_hubbedlb'])){
        unset($_SESSION['hubbed_hubbedlb']);
    }



global $woocommerce;
        $woocommerce->customer->set_shipping_first_name('');
        $woocommerce->customer->set_shipping_last_name('');
        $woocommerce->customer->set_shipping_company('');
        $woocommerce->customer->set_shipping_address_1('');
        $woocommerce->customer->set_shipping_address_2('');
        $woocommerce->customer->set_shipping_country('');
        $woocommerce->customer->set_shipping_state('');
        $woocommerce->customer->set_shipping_city('');
        $woocommerce->customer->set_shipping_postcode('') ;
  }



  // Shipping Calculation 

add_action('woocommerce_cart_calculate_fees', function() {

if(Is_checkout())
{

  session_start();

 if(get_option('hubbed_shipping_fee') == 1)
 { 
  $hubbed_cutoff_price = get_option('hubbed_cutoff_price');
  $hubbed_lower_price = get_option('hubbed_lower_price');
  $hubbed_higher_price = get_option('hubbed_higher_price');
  $subtotal = WC()->cart->subtotal;
  
  if(isset($_SESSION['hubbed_address']) && !empty($_SESSION['hubbed_address']))
  {
    if ($subtotal < $hubbed_cutoff_price) 
    {
      if (!empty($hubbed_lower_price)) 
      {
      WC()->cart->add_fee('Click & Collect Shipping Fee', $hubbed_lower_price);
      }

    }else
    {
      if (!empty($hubbed_higher_price)) 
      {
      WC()->cart->add_fee('Click & Collect Shipping Fee', $hubbed_higher_price);
      } 
      
    }
  
  
  }


} // Check shipping enable or not
}
});