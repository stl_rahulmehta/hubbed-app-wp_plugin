=== HUBBED ===
Contributors: hubbedwordpress
Donate link: https://hubbed.com
Tags: hubbed, clickandcollect, collect, collectpointnetworks, parcelmanagementsolutions, eCommerce, carrierindustry
Requires at least: 3.5
Tested up to: 5.5
Stable tag: trunk
Requires PHP: 5.0
License: later

Click & Collect made easy 
Enable the convenience of customer choice with the HUBBED Click & Collect WooCommerce plugin.  

== Description ==

HUBBED is a national parcel pick-up and drop-off network (PUDO) with 2000+ trusted collection points Australia wide. Our Easy Click & Collect plugin lets you add local HUBBED collection points to your WooCommerce store shopping cart. 
Improve customer satisfaction and shopping cart conversion with HUBBED Click & Collect. Our WooCommerce plugin gives your customers more delivery choices with secure and extended hours collection points.  

HUBBED collection point benefits:  

* Secure parcel collection management 

* 24 hour and extended hour pick up locations  

* Established retail brands including 7-Eleven, BP, National Storage, Snap Printing, Pack Send and Repco 

* Local destinations 

* Works with your existing logistics provider 

* Zero operational changes 

* No more missed deliveries with guaranteed delivery 

* Reduced carbon footprint by consolidating deliveries  

HUBBED plugin features:  

* 2000+ secure and extended hour collection points  

* Smart parcel lockers 

* Unlimited parcel transactions  

* Collection point finder map 

* Parcel collection notifications via email & SMS  

* Unique digital collection PIN code for added security 

== How do I launch Click & Collect by HUBBED? == 

To use the HUBBED Click & Collect WooCommerce plugin in your store, you must register for a HUBBED Account at <a href="http://clickandcollect.hubbed.com">http://clickandcollect.hubbed.com.</a>  
After creating a HUBBED account, we will send you an API activation key.  
Use this unique API key to validate your account when installing the WooCommerce HUBBED plugin.  
 
Once the plugin has successfully been installed, activate Click & Collect in your shopping cart and go live. Remember to update your delivery policies and tell your customers about Click & Collect. 

== Pricing == 

30 day free trial. Per parcel handling fees apply    

A monthly network access fee will apply after the first month. See <a href="http://clickandcollect.hubbed.com/pricing/">http://clickandcollect.hubbed.com/pricing/</a> 
 

== Installation ==

= Step Follow =

1. At least 1 Shipping zone added with method.
2. Cart & Checkout Page set.
3. Key must insert in hubbed setting page.(Testing key - 4d5991dd-4a7c-42fb-93f2-a7e7dfab463e)

= Automatic installation =

Automatic installation is the easiest option — WordPress will handles the file transfer, and you won’t need to leave your web browser. To do an automatic install of HUBBED, log in to your WordPress dashboard, navigate to the Plugins menu, and click “Add New.”

In the search field type HUBBED,” then click “Search Plugins.” Once you’ve found us, you can view details about it such as the point release, rating, and description. Most importantly of course, you can install it by! Click “Install Now,” and WordPress will take it from there.

= Manual installation =

Manual installation method requires downloading the HUBBED plugin and uploading it to your web server via your favorite FTP application.


== Frequently Asked Questions ==
1. Where can I find my HUBBED API Key?  

We will send you a unique API key when you sign up for a HUBBED account. Visit <a href="http://clickandcollect.hubbed.com/">http://clickandcollect.hubbed.com/ </a>
You will also be able to find your API when you login to your HUBBED account.  

2. How many transactions can I send per month?  

There is no limit to the number of parcels you ship to HUBBED collection points. A parcel handling fee will apply per parcel.  


== Screenshots ==

1. Add HUBBED API key in HUBBED Settings.
2. Guide for how to install & setup the plugin.
3. Display Click & Collect button in the cart page.
4. Click and Collect button - Open the map/list view.
5. Search using either Suburb and postcode
6. Select Click & Collect location for shipping.
7. How to check order click & collect status (WooCommerce -> Orders -> Edit order)


== Additional Info ==

= 3rd Party Service =

1. When plugin Install we store site URL, site name & API key on our server, these details we verify on every Address search on Cart/Checkout page.
2. Once order placed with Hubbed Address only we share details like Order ID, cart Items and customer details like Customer Name, Contact Number and Email to our server.
3. On order dispatch We also use same (Order & Customer) details to inform our Hubbed location.
4. Privacy policy URL:-<a href="https://hubbed.com/privacy-policy/"> https://hubbed.com/privacy-policy/</a>


== Support==