				const closeIcon = '<svg fill="#f47521" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"> <g> <path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/> </g> </svg>';
	            	const searchIcon = '<svg version="1.1" fill="#fff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 512 512"> <g> <path d="M495,466.2L377.2,348.4c29.2-35.6,46.8-81.2,46.8-130.9C424,103.5,331.5,11,217.5,11C103.4,11,11,103.5,11,217.5 S103.4,424,217.5,424c49.7,0,95.2-17.5,130.8-46.7L466.1,495c8,8,20.9,8,28.9,0C503,487.1,503,474.1,495,466.2z M217.5,382.9 C126.2,382.9,52,308.7,52,217.5S126.2,52,217.5,52C308.7,52,383,126.3,383,217.5S308.7,382.9,217.5,382.9z"/> </g> </svg>';
	            	const pinIcon = '<svg fill="#757575" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g> <path d="M308.2,364.465l79.247-144.236c11.698-21.667,17.885-46.177,17.885-70.896C405.333,66.99,338.344,0,256,0 S106.667,66.99,106.667,149.333c0,24.719,6.188,49.229,17.927,70.958l79.208,144.173c-69.939,4.439-182.469,19.813-182.469,72.869 C21.333,509.906,231.979,512,256,512s234.667-2.094,234.667-74.667C490.667,384.277,378.139,368.904,308.2,364.465z M170.667,149.333C170.667,102.281,208.948,64,256,64s85.333,38.281,85.333,85.333c0,47.052-38.281,85.333-85.333,85.333 S170.667,196.385,170.667,149.333z M256,469.333c-104.885,0-173.667-19.177-189.781-32 c14.548-11.577,72.133-28.284,160.348-31.431l20.09,36.566c1.865,3.417,5.448,5.531,9.344,5.531c3.896,0,7.479-2.115,9.344-5.531 l20.091-36.566c88.214,3.147,145.798,19.854,160.346,31.431C429.667,450.156,360.885,469.333,256,469.333z"/> </g> </svg>';
	            	const locationIcon = '<svg viewBox="-26 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m109.921875 204.855469c0 52.34375 42.585937 94.933593 94.933594 94.933593 52.34375 0 94.933593-42.589843 94.933593-94.933593 0-52.347657-42.589843-94.933594-94.933593-94.933594-52.347657 0-94.933594 42.585937-94.933594 94.933594zm149.894531 0c0 30.304687-24.65625 54.960937-54.960937 54.960937-30.304688 0-54.960938-24.65625-54.960938-54.960937 0-30.304688 24.65625-54.960938 54.960938-54.960938 30.304687 0 54.960937 24.65625 54.960937 54.960938zm200.738282 280.4375-55.773438-62.109375c10.640625-14.773438 16.921875-32.890625 16.921875-52.445313 0-49.59375-40.347656-89.9375-89.9375-89.9375s-89.9375 40.34375-89.9375 89.9375c0 49.589844 40.347656 89.933594 89.9375 89.933594 15.59375 0 30.273437-3.992187 43.074219-11.003906l55.976562 62.332031zm-178.753907-114.554688c0-27.550781 22.414063-49.964843 49.964844-49.964843s49.964844 22.414062 49.964844 49.964843-22.414063 49.964844-49.964844 49.964844-49.964844-22.414063-49.964844-49.964844zm-31.25 101.3125c-18.992187 17.671875-32.121093 28.332031-33.136719 29.152344l-12.558593 10.144531-12.5625-10.144531c-1.941407-1.570313-48.113281-39.058594-94.941407-92.824219-64.597656-74.164062-97.351562-140.734375-97.351562-197.859375v-5.664062c0-112.957031 91.898438-204.855469 204.855469-204.855469s204.855469 91.898438 204.855469 204.855469v5.664062c0 16.203125-2.644532 33.171875-7.894532 50.859375-11.234375-7.222656-23.652344-12.753906-36.886718-16.25 3.082031-11.707031 4.808593-23.300781 4.808593-34.609375v-5.664062c0-90.917969-73.96875-164.882813-164.882812-164.882813-90.917969 0-164.882813 73.964844-164.882813 164.882813v5.664062c0 96.535157 124.378906 213.394531 164.882813 248.882813 5.054687-4.429688 11.417969-10.125 18.683593-16.894532 7.441407 11.183594 16.566407 21.152344 27.011719 29.542969zm0 0"/></svg>';
	            	const giftIcon = '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g> <path d="M480,143.686H378.752c7.264-4.96,13.504-9.888,17.856-14.304c25.792-25.952,25.792-68.192,0-94.144 c-25.056-25.216-68.768-25.248-93.856,0c-13.856,13.92-50.688,70.592-45.6,108.448h-2.304 c5.056-37.856-31.744-94.528-45.6-108.448c-25.088-25.248-68.8-25.216-93.856,0C89.6,61.19,89.6,103.43,115.36,129.382 c4.384,4.416,10.624,9.344,17.888,14.304H32c-17.632,0-32,14.368-32,32v80c0,8.832,7.168,16,16,16h16v192 c0,17.632,14.368,32,32,32h384c17.632,0,32-14.368,32-32v-192h16c8.832,0,16-7.168,16-16v-80 C512,158.054,497.632,143.686,480,143.686z M138.08,57.798c6.496-6.528,15.104-10.112,24.256-10.112 c9.12,0,17.728,3.584,24.224,10.112c21.568,21.696,43.008,77.12,35.552,84.832c0,0-1.344,1.056-5.92,1.056 c-22.112,0-64.32-22.976-78.112-36.864C124.672,93.318,124.672,71.302,138.08,57.798z M240,463.686H64v-192h176V463.686z M240,239.686H32v-64h184.192H240V239.686z M325.44,57.798c12.992-13.024,35.52-12.992,48.48,0 c13.408,13.504,13.408,35.52,0,49.024c-13.792,13.888-56,36.864-78.112,36.864c-4.576,0-5.92-1.024-5.952-1.056 C282.432,134.918,303.872,79.494,325.44,57.798z M448,463.686H272v-192h176V463.686z M480,239.686H272v-64h23.808H480V239.686z"/> </g> </svg>';
	            	const collectIcon = '<svg height="511pt" viewBox="1 -58 511.999 511" width="511pt" xmlns="http://www.w3.org/2000/svg"><path d="m15 395.421875c-8.285156 0-15-6.714844-15-15v-46.734375c0-45.664062 30.15625-84.492188 74.457031-95.84375l51.867188-13.292969c6.171875-1.574219 12.429687.949219 15.898437 5.859375l32.949219 43.421875 32.894531-43.34375c3.722656-4.910156 9.894532-6.867187 15.515625-5.464843l51.863281 12.984374c7.640626 1.917969 14.851563 4.644532 21.554688 8.078126v-50.585938c0-8.285156 6.714844-15 15-15h185c8.28125 0 15 6.714844 15 15v185c0 8.285156-6.71875 15-15 15-160.667969 0-321.332031-.078125-482-.078125zm160.171875-394.921875c-55.777344 0-101 45.222656-101 101s45.222656 101 101 101 101-45.222656 101-101-45.222656-101-101-101zm50.207031 50.792969c27.726563 27.726562 27.726563 72.6875 0 100.414062-27.726562 27.722657-72.683594 27.722657-100.410156 0-27.726562-27.726562-27.726562-72.6875 0-100.414062 27.726562-27.722657 72.683594-27.722657 100.410156 0zm225.015625 159.207031v49.464844c0 10.601562-11.316406 19.265625-22.949219 12.71875l-22.851562-11.484375-24.121094 12.125c-10.042968 5.035156-21.628906-2.492188-21.621094-13.359375l-.058593-49.464844h-31.792969v155h155v-155zm-30 2h-31.601562v23.210938c16.535156-8.3125 14.90625-8.390626 31.601562 0zm-123.394531 69.460938c-8.210938-7.101563-18.105469-12.199219-28.820312-14.890626l-42.320313-10.632812c-5.511719 7.261719-38 51.210938-41.664063 53.984375-6.566406 4.984375-15.933593 3.699219-20.917968-2.871094l-39.15625-51.597656-42.28125 10.835937c-31.023438 7.949219-51.839844 34.8125-51.839844 66.898438v31.734375h267zm0 0" fill-rule="evenodd"/></svg>';

window.infowindow_active = false;
window.previous_marker = previous_marker_img = false;
jQuery( document ).ready(function() {

jQuery(".hubbed-pin-location").html(pinIcon);
jQuery("#hubbed-search-button-first").html(searchIcon);
jQuery(".hubbed-steper-icon-location").html(locationIcon)
jQuery(".hubbed-steper-icon-send").html(giftIcon)
jQuery(".hubbed-steper-icon-collect").html(collectIcon)
jQuery(".hubbed-search-button").html(searchIcon);

jQuery(document).on("click", "#hubbed-btn-click-collect",function(){
jQuery(".hubbed-modal").addClass("active");

var hubbed_first_phase = jQuery('.hubbed-first-phase');
	hubbed_first_phase[0].style.display = 'block';
var hubbed_first_phase = jQuery('.hubbed-second-phase');
	hubbed_first_phase[0].style.display = 'none';
  
  	jQuery('.hubbed-modal').addClass('hubbed-is-visible');
  	jQuery('body').addClass('hubbed-scrollfix');

if(!window.bounds)
{
window.bounds = new google.maps.LatLngBounds();
}
if(!window.markers){
window.markers = [];
}
if(!window.hubbed_map){
		window.hubbed_map = new google.maps.Map(document.getElementById('hubbed-map'),{
			center: { lat: -26.1772288, lng: 133.4170119 },
    		zoom: 4,
			draggable: 1,
			zoomControl: 1,
			zoomControlOptions: {
				position: google.maps.ControlPosition.RIGHT_CENTER
			},
			mapTypeControl: 0,
			streetViewControl: 0,
			fullscreenControl: 0,
			gestureHandling: "greedy",
			styles: [
				{
				  featureType: "poi.business",
				  stylers: [{ visibility: "off" }]
				},
				{
				  featureType: "transit",
				  elementType: "labels.icon",
				  stylers: [{ visibility: "off" }]
				},
				{ 
					featureType: "building",
					elementType: "labels.icon",
        			stylers: [ { visibility: "off" }]
      }

			  ]
			
		});
	}

});

jQuery(document).on("click",".hubbed-close", function() {
  jQuery(".hubbed-modal").removeClass("active");
  jQuery(".hubbed-modal").removeClass("hubbed-is-visible");
  jQuery('body').removeClass('hubbed-scrollfix');


});


});

function LoaderShow()
{
  	jQuery('#hubbedModelLoaderBackgroud').css('display','flex');
  	jQuery('body').addClass("hubbed-loader-block");
}

function LoaderHide(){
  	jQuery('#hubbedModelLoaderBackgroud').css('display','none');
	jQuery('body').removeClass('hubbed-loader-block');
}


function showhidemap(show){
	
	if(show == 1){	
		jQuery("#hubbed_display_response").removeClass('hubbed-show-list');
		jQuery("#hubbed-explore-toggle-list").removeClass('active');
		jQuery("#hubbed-explore-toggle-map").addClass('active');
	}else{
		jQuery("#hubbed_display_response").addClass('hubbed-show-list');
		jQuery("#hubbed-explore-toggle-list").addClass('active');
		jQuery("#hubbed-explore-toggle-map").removeClass('active');
	}

};


function hubbedlisting(searched_nonce,searched_postcode,searched_services,searched_channel,searched_radius,page_no,append=0)
{
	LoaderShow();
jQuery.ajax({
        type : "post",
        dataType : "json",
        url : hubbed_ajax.ajaxurl,
        data : {action: "searched_list", searched_postcode:searched_postcode,searched_services:searched_services,searched_channel:searched_channel,searched_nonce:searched_nonce, searched_radius:searched_radius,page_no:page_no },

        success: function(response) 
        {
	LoaderHide();
        var responsedata = JSON.stringify(response);
//console.log(response);
		var returnedData = response.searchresult;

         if (returnedData.responseMessage == "Success") {
				var append_html = "";


                jQuery(returnedData.data).each(function(index, element){
					var address2 = "";

                    var address = element['address']['street1'] + ', ' +
                    address2 +
                    element['address']['city'] + ', ' +
                    element['address']['state'] + ', ' +
                    element['address']['country'] + ', ' +
                    element['address']['postcode'];

					var map_button = '<button  class="hubbed_selected_item hubbed-button-same select-address" type="button" data-company="' + element['name'].replace(/&/g, "and") + '" data-address="'+element['address']['street1'].replace(/&/g, "and")+'" data-address2="'+address2+'" data-city="'+element['address']['city']+'" data-province="'+element['address']['state']+'" data-country="'+element['address']['country']+'" data-zip="'+element['address']['postcode']+'" data-hubbedlb="'+element['storeDlb']+'" ><span class="dashicons dashicons-arrow-right-alt"></span></button>';

					var map_html = '<div class="hubbed-map-address"> <h4 class="hubbed-Name hubbed-h4-heading">'+element['name']+'</h4><p class="hubbed-address hubbed-parapgraph-text"><span class="dashicons dashicons-building"></span>'+address+'</p><p class="timing hubbed-parapgraph-text"><span class="dashicons dashicons-clock"></span>'+element['operatingHours']+'</p>'+map_button+'</div>'

					var infowindow = new google.maps.InfoWindow({
						content: map_html
					  });
					var main_marker = "https://app.hubbed.com.au/storage/shopify/images/google-place.png";
					var marker = new google.maps.Marker({
						position: {lat: element['latitude'], lng: element['longitude']},
						map: hubbed_map,
						icon: element['channel']['marker_inactive']
						//icon: main_marker
					});
					marker.addListener('click', function() {
						//infowindow.open(hubbed_map, marker);
						previous_marker && previous_marker.setIcon(previous_marker_img);
						previous_marker = marker;
						marker.setIcon(element['channel']['marker_active']);
						infowindow_active && infowindow_active.close();
						previous_marker_img = element['channel']['marker_inactive'];
						infowindow.open(hubbed_map, marker);
						infowindow_active = infowindow;

						});
					google.maps.event.addListener(infowindow,'closeclick',function(){
						marker.setIcon(element['channel']['marker_inactive']);
					});
					window.markers.push(marker);
					bounds.extend(new google.maps.LatLng(element['latitude'],element['longitude']));

                	address2 = '';
                    /*if (element['address']['street2'] != null || element['address']['street2'] != '' || element['address']['street2'] != 'null'){
                      address2 = element['address']['street2'] + ', ';
                    }*/


                    append_html += '<div class="hubbed-show-address"><div class="hubbed-left"><h4 class="hubbed-Name hubbed-h4-heading">' + element['name'] + '</h4><p class="hubbed-address hubbed-parapgraph-text"><span class="dashicons dashicons-building"></span>' + address + '</p><p class="timing hubbed-parapgraph-text"><span class="dashicons dashicons-clock"></span> ' + element['operatingHours'] + '</p></div><div class="hubbed-right"><button  class="hubbed_selected_item hubbed-button-same select-address" type="button" data-company="' + element['name'].replace(/&/g, "and") + '" data-address="'+element['address']['street1'].replace(/&/g, "and")+'" data-address2="'+address2+'" data-city="'+element['address']['city']+'" data-province="'+element['address']['state']+'" data-country="'+element['address']['country']+'" data-zip="'+element['address']['postcode']+'" data-hubbedlb="'+element['storeDlb']+'" ><span class="dashicons dashicons-arrow-right-alt"></span></button></div></div>';
				});
				hubbed_map.fitBounds(bounds);
                document.getElementById("hubbed_display_showmorebtn").innerHTML = '';
                
                if (returnedData.totalPages > 1) {
                  if(page_no < returnedData.totalPages){
                    var nextPage = parseInt(page_no) + 1;
                    document.getElementById("hubbed_display_showmorebtn").innerHTML = "<div class='hubbed-loadmore-section'><div class='hubbed-right'><span class='hubbed-button-same load_more hubbed_loadmore hubbed-button-same' id='load_more' data-page='"+ nextPage +"' data-searchKeyword='"+ searched_postcode +"' data-searchedChannel='"+ searched_channel +"' data-searchedServices='"+ searched_services +"' data-searchedNonce='"+searched_nonce+"'>Load More</span></div></div>";
                  }else{
                    document.getElementById("hubbed_display_showmorebtn").innerHTML = "<div class='hubbed-loadmore-section'><div class='hubbed-right'>-- No More records --</div></div>";
                  }

                }

                if(append == 1)
                {
                    document.getElementById("hubbed_display_records").innerHTML += append_html;
                }
                else{
                 	document.getElementById("hubbed_display_records").innerHTML = append_html;   
                }
                
                
            }
            else
            {
                document.getElementById("hubbed_display_records").innerHTML =  
'<div class="hubbed-show-address"><p><b>We are sorry. A pickup store is not available within your search area. Please increase the radius of the search or search for an alternate location.</b></p></div>';   
jQuery("#hubbed-explore-toggle-list").click();
document.getElementById("hubbed_display_showmorebtn").innerHTML ="";
for (var i = 0; i < window.markers.length; i++) {
window.markers[i].setMap(null);
}

window.markers = [];
window.bounds = new google.maps.LatLngBounds();

            
            }
           
           
           }

	});
 
}

// Click on show more

jQuery(document).on('click', '#hubbed-display-showmorebtn', function(){
var page_no = parseInt(jQuery(this).attr('data-page'));
console.log(page_no);
var next_page_no = page_no + 1;
console.log(next_page_no);

jQuery(this).attr('data-page',next_page_no);

	var  searched_postcode = jQuery("#hubbed-search-field-input").val();
    var searched_services = jQuery(".hubbed_filter_servises").val();
    var searched_channel = jQuery(".hubbed_filter_channel").val();
    var searched_radius = jQuery('input[name="hubbed_radius"]:checked').val();
    var  searched_nonce = jQuery("#search_nonce_postcode").val();
    var page_no = jQuery('#hubbed-display-showmorebtn').attr('data-page');


//hubbedlisting(searched_postcode,searched_services,searched_channel,searched_radius,page_no);

});


// click on search button

jQuery(document).on('keyup',"#hubbed-search-field-one" ,function(event) {
    if (event.keyCode === 13) {
        jQuery("#hubbed-search-button-first").click();
    }
});
jQuery(document).on('click', '#hubbed-search-button-first', function()
{


var hubbed_first_phase = jQuery('.hubbed-first-phase');
	hubbed_first_phase[0].style.display = 'none';
var hubbed_first_phase = jQuery('.hubbed-second-phase');
	hubbed_first_phase[0].style.display = 'block';

	var  searched_postcode = jQuery("#hubbed-search-field-one").val();
    var searched_services = '';
    var searched_channel = '';
    //var searched_radius = 3;
    var searched_radius = jQuery('input[name="hubbed_radius"]:checked').val();
    var  searched_nonce = jQuery("#search_nonce_postcode").val();
    var page_no = jQuery('#load_more').attr('data-page');
    page_no = page_no  - 1;
    if (page_no.length == null) {
    	page_no = 1;
    }
	var  searched_nonce = jQuery("#search_nonce_postcode").val();
	jQuery("#hubbed-search-field-two").val(searched_postcode);
  


    hubbedlisting(searched_nonce,searched_postcode,searched_services,searched_channel,searched_radius,page_no,append=0)

  });  


jQuery(document).on('click', '#load_more', function()
{

	var page_no = jQuery(this).attr("data-page");
	var searched_postcode = jQuery(this).attr("data-searchKeyword");
	var searched_services = jQuery(this).attr("data-searchedServices");
	var searched_channel = jQuery(this).attr("data-searchedChannel");
	var searched_nonce = jQuery(this).attr("data-searchedNonce");
	//var searched_radius = 3;
	var searched_radius = jQuery('input[name="hubbed_radius"]:checked').val();
  	
  	hubbedlisting(searched_nonce,searched_postcode,searched_services,searched_channel,searched_radius,page_no,append=1)
});

jQuery("#hubbed-search-field-two").keyup(function(event) {
    if (event.keyCode === 13) {
        jQuery("#hubbed-search-button-second").click();
    }
});
jQuery(document).on('click', '#hubbed-search-button-second', function()
{
	var  searched_postcode = jQuery("#hubbed-search-field-two").val();
for (var i = 0; i < window.markers.length; i++) {
window.markers[i].setMap(null);
}
window.markers = [];
window.bounds = new google.maps.LatLngBounds();

            
            
	var page_no = 1;
	var searched_radius = jQuery('input[name="hubbed_radius"]:checked').val();
	var searched_services = jQuery('input[name="hubbed_filter_service"]:selected').val();
	var searched_channel = jQuery('input[name="hubbed_filter_channel"]:selected').val();
	var  searched_nonce = jQuery("#search_nonce_postcode").val();
	if (searched_services == null) { searched_services="";}
	if (searched_channel == null) { searched_channel="";}
	
	
  	
  	hubbedlisting(searched_nonce,searched_postcode,searched_services,searched_channel,searched_radius,page_no,append=0)


});


function hubbedselectAddress(data)
{
	LoaderShow();
var address = jQuery(data).attr("data-address");
var company = jQuery(data).attr("data-company");
var address2 = jQuery(data).attr("data-address2");
var city = jQuery(data).attr("data-city");
var state = jQuery(data).attr("data-province");
var country = jQuery(data).attr("data-country");
var zip = jQuery(data).attr("data-zip");
var hubbedlb = jQuery(data).attr("data-hubbedlb");

jQuery.ajax({
        type : "post",
        dataType : "json",
        url : hubbed_ajax.ajaxurl,
        data : {action: "hubbed_selected_address", company:company, address:address,address2:address2,city:city,state:state, country:country,zip:zip,hubbedlb:hubbedlb },

        success: function(response) 
        {
        		LoaderHide();
        	var responsedata = JSON.stringify(response);
        	window.location.href=response.checkout_url;
        }
    })

};

jQuery(document).on('click', '.select-address', function()
{
	hubbedselectAddress(this);

});	



jQuery(document).on('click', '#cross_hubbed_checkout_address', function()
{


jQuery.ajax({
        type : "post",
        dataType : "json",
        url : hubbed_ajax.ajaxurl,
        data : {action: "removed_hubbed_address"},

        success: function(response) 
        {
          jQuery('.hubbed_checkout_address').remove();
         // jQuery("#ship-to-different-address-checkbox"). prop("checked", false);


}
});
});